App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  loading: false,
  tokenPrice: 1000000000000000,
  tokensSold: 0,
  crowdSaleAddr: 0,
  crowdSaleBal: 0,
  crowdSaleFixedToken: 750000,

  init: function() {
    console.log("App initialized...")
    return App.initWeb3();
  },

  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.

      web3 = new Web3(web3.currentProvider);
      App.web3Provider = web3.currentProvider;
      console.log("web3 injected.")
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContracts();
  },

  initContracts: function() {
    console.log('starting contracts...')
    $.getJSON("../contracts/DappTokenSale.json", function(dappTokenSale) {
      App.contracts.DappTokenSale = TruffleContract(dappTokenSale);
      console.log('starting Truffle contacted');
      App.contracts.DappTokenSale.setProvider(App.web3Provider);
      console.log('Provider Set');
      App.contracts.DappTokenSale.deployed().then(function(dappTokenSale) {
      App.crowdSaleAddr = dappTokenSale.address;
      console.log("Dapp Token Sale Address: ", App.crowdSaleAddr);
      });
    }).done(function() {
      $.getJSON("../contracts/DappToken.json", function(dappToken) {
        App.contracts.DappToken = TruffleContract(dappToken);
        App.contracts.DappToken.setProvider(App.web3Provider);
        App.contracts.DappToken.deployed().then(function(dappToken) {
        });
        console.log('contracts...started?')
        App.listenForEvents();
        console.log('Listening for events?')
        return App.render();
      });
    })
  },

  // Listen for events emitted from the contract
  listenForEvents: function() {
    App.contracts.DappTokenSale.deployed().then(function(instance) {
      instance.Sell({}, {
        fromBlock: 0,
        toBlock: 'latest',
      }).watch(function(error, event) {
        console.log("event triggered", event);
        App.render();
      })
    })
  },

  render: function() {

    console.log('Rendering...')

    if (App.loading) {
      console.log('panic!')
      return;
    }
    App.loading = true;

    var loader  = $('#loader');
    var content = $('#content');

    loader.show();
    content.hide();

    // Load account data
    web3.eth.getCoinbase(function(err, account) {
      if(err === null) {
        App.account = account;
        $('#accountAddress').html("Your Account: " + account);
      }
    })


    // Load token sale contract
    App.contracts.DappTokenSale.deployed().then(function(instance) {

      dappTokenSaleInstance = instance;

      return dappTokenSaleInstance.tokenPrice();
    }).then(function(tokenPrice) {
      App.tokenPrice = tokenPrice;

      $('.token-price').html(web3.fromWei(App.tokenPrice, "ether").toNumber());
      return dappTokenSaleInstance.tokensSold();
    }).then(function(tokensSold) {
      App.tokensSold = tokensSold.toNumber();
      $('.tokens-sold').html(App.tokensSold);
      // Load token contract
      App.contracts.DappToken.deployed().then(function(instance) {
        dappTokenInstance = instance;
        return dappTokenInstance.balanceOf(App.account);
      }).then(function(balance) {

        var progressPercent = (Math.ceil(App.tokensSold) / App.crowdSaleFixedToken) * 100;

        $('.dapp-balance').html(balance.toNumber()); //display metamask user token balance.
        $('#progress').css('width', progressPercent + '%');
        $('.crowdsale-address').html('Crowdsale address: ' + App.crowdSaleAddr);
        $('.tokens-available').html( App.crowdSaleFixedToken);
        console.log(progressPercent);
        console.log('Crowdsale Address', App.crowdSaleAddr);
        console.log('Crowdsale Balance: ', App.crowdSaleBal);
        App.loading = false;
        loader.hide();
        content.show();
      })
    });
  },

  buyTokens: function() {


    $('#content').hide();
    $('#loader').show();
    var numberOfTokens = $('#numberOfTokens').val();
    App.contracts.DappTokenSale.deployed().then(function(instance) { //contract?
      return instance.buyTokens(numberOfTokens, {
        from: App.account,
        value: numberOfTokens * App.tokenPrice,
        gas: 250000, // Gas limit
        nonce: 41
      });
    }).then(function(result) {
      console.log("Tokens bought...")
      $('form').trigger('reset') // reset number of tokens in form
      // Wait for Sell event
    });
  },

  transfer: function() {


    $('#content').hide();
    $('#loader').show();
    var crowdSaleInfo = $('#contractAddr').val();
    var numberOfTokens = $('#numberOfTokens').val();

    App.contracts.DappToken.deployed().then(function(instance) {
      return instance.transfer(crowdSaleInfo, numberOfTokens, {
        from: App.account,
        gas: 250000 // Gas limit
      });
    }).then(function(result) {
      console.log("Tokens Sent...")
      $('form').trigger('reset') // reset number of tokens in form
      // Wait for Sell event
    });
  }
  ,

  checkBalance: function() {

    var address = $('#checkAddr').val();

    App.contracts.DappToken.deployed().then(function(instance) {
        dappTokenInstance = instance;

      return dappTokenInstance.balanceOf(address);
    }).then(function(balance) {
            $('.accountBalance').html("Token Balance:" + balance.toNumber() );
      //$('.accountBalance').html(balance.toNumber());

    });
  }
}
  $(function() {
    $(window).load(function() {
      App.init();
    })
  });

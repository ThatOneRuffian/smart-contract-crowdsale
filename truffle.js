module.exports = {
  networks: {
    local: {
      host: "192.168.1.252",
      port: 7545,
      network_id: "5777" // Match any network id
    },
    Ropsten: {
      host: "192.168.1.250",
      port: 8545,
      network_id: "3" // Ropsten network id
    }

  }
};
